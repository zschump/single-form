import React from 'react';
import { connect } from 'react-redux';

class Results extends React.Component {
  constructor() {
    super();
  }

  generateResultsObject() {
    if (this.props.simple) {
      if (this.props.simple.values) {
        return this.props.simple.values;
      }
    }
  }

  createResultsArray(resultsObject) {
    const resultsArray = [];

    if (resultsObject) {
      for (const key in resultsObject) {
        if (resultsObject.hasOwnProperty(key)) {
          const element = resultsObject[key];

          resultsArray.push({
            key,
            value: element
          });
        }
      }
    }
    return resultsArray;
  }

  mapResultsArray(resultsArray) {
    return resultsArray.map((result, index) => {
      return (
        <div key={index}>
          <p>
            {result.key}: {result.value}
          </p>
        </div>
      );
    });
  }

  render() {
    console.log(this.createResultsArray(this.generateResultsObject()));
    return (
      <div>
        {this.mapResultsArray(
          this.createResultsArray(this.generateResultsObject())
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  simple: state.form.simple
});

export default connect(mapStateToProps)(Results);
