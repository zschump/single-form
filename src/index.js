import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
// import { Values } from "redux-form-website-template";
import store from "./store";
import showResults from "./showResults";
import SimpleForm from "./simpleForm";
import Results from "./results";

const rootEl = document.getElementById("root");

ReactDOM.render(
    <Provider store={store}>
        <div style={{ padding: 15 }}>
            <h2>SimpleForm</h2>
            <SimpleForm onSubmit={showResults} />
            <Results/>
        </div>
        <div>
        {/* `${JSON.stringify(values, null, 2)}` */}
        </div>
    </Provider>,
   rootEl
);
